# Hafiz (Personal Page(s))

Here I have my personal page having information about myself and that which I wish to share with the world.

The code is free and open source, you can clone this project, dissect it, use it however you wish.

The content is authored by me, unless otherwise specified. Where other people have their content with specific license differing from my chosen one (MIT), I will specify accordingly. This applies for blog posts, and perhaps books (and other content) that others author, that I might share here.

Respect the rules and use accordingly
