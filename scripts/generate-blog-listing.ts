import * as prettier from 'prettier';
import { FC as ReactFC } from 'react';
import { resolve as resolvePath, parse as parsePath } from 'path';
import { readdirSync, writeFileSync } from 'fs';
import { fileURLToPath } from 'url';
import { dirname } from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

interface IBlogPageImport {
  title: string;
  summary: string;
  default: ReactFC;
}

interface IBlogPostData extends Omit<IBlogPageImport, 'default'> {
  slug: string;
  component: string;
}

const srcPath = resolvePath(__dirname, '..', 'src');
const blogPostsPath = resolvePath(srcPath, 'blog-posts');
const items = readdirSync(blogPostsPath);

console.log(items);

const listing: IBlogPostData[] = [];
for (const i of items) {
  const imported = (await import(
    resolvePath(blogPostsPath, i)
  )) as IBlogPageImport;
  listing.push({
    title: imported.title,
    summary: imported.summary,
    slug: parsePath(i).name,
    component: `import('../blog-posts/${i}')`,
  });
}

console.log(listing);

let arrString = `[\n`;
for (const item of listing) {
  arrString += ` {
    title: "${item.title}",
    summary: \`${item.summary}\`,
    slug: "${item.slug}",
    component: ${item.component},
  },`;
}

arrString += '\n];\n';

const toWrite = `
export default ${arrString}
`;

writeFileSync(
  resolvePath(srcPath, 'config', 'blog-post-config.ts'),
  await prettier.format(toWrite, { parser: 'babel-ts' })
);

export default {};
