import { resolve as resolvePath } from 'path';
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import sassDts from 'vite-plugin-sass-dts';

const srcPath = resolvePath(__dirname, 'src');

function genPath(...pieces: string[]) {
  return resolvePath(srcPath, ...pieces);
}

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), sassDts()],
  resolve: {
    alias: {
      '@src': srcPath,
      '@assets': genPath('assets'),
      '@components': genPath('components'),
      '@site-pieces': genPath('site-pieces'),
      '@blog-posts': genPath('blog-posts'),
      '@routes': genPath('routes'),
      '@pages': genPath('pages'),
      '@types': genPath('types'),
    },
  },
});
