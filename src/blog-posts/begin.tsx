import BlogPostLayoutGeneral from '@src/components/BlogPostLayoutGeneral';
import quoteFirstVersionSrc from '@assets/quotes/material_world/reid_hoffman/first-version.jpg';
import quoteAssembleAirplaneSrc from '@assets/quotes/material_world/reid_hoffman/assemble-airplane.jpg';

export const title = 'Beginning of My Website';
export const summary = `
The beginning of this project. It's astute to kick it off with a beginning blog post. As I should.
`;

export default function Begin() {
  return (
    <BlogPostLayoutGeneral title="The Beginning">
      <p>Welcome to my blog. To kick off this project, I begin with a post.</p>
      <p>
        I will begin with the onset and say I intend to be different from others
        who engage in the occult. Their views, if you meet them on places such
        as{' '}
        <a href="https://discord.com/" target="_blank">
          discord
        </a>
        , might generally differ in a considerable extent from mine. There are
        quite a lot of things that people are not affected by, and by this I
        refer to general phenomenon. One thing you'll come across is the concept
        of 'the veil'. However, differing from general views and not wishing to
        cause potential discomfort for those whose views might be similar to
        mine, I refrain from expounding on this matter.
      </p>
      <p>
        I will be making frequent references to posts from businessmen and
        others where said words are meant to act as inspiration. Worry not of an
        elitst frame, or of the entirety of the character of the individuals,
        learn and use what you can.
      </p>
      <p>So let me begin with this:</p>
      <p>
        <img
          src={quoteFirstVersionSrc}
          alt="If you are not embarrassed by the first version of your product, you’ve launched too late."
        />
      </p>
      <p>But also this:</p>
      <p>
        <img
          src={quoteAssembleAirplaneSrc}
          alt="You jump off a cliff and you assemble an airplane on the way down."
        />
      </p>
      <p>There is much to come. All in due time.</p>
      <p style={{ textAlign: 'right' }}>- Hafiz / حافظ</p>
    </BlogPostLayoutGeneral>
  );
}
