export default [
  {
    title: 'Beginning of My Website',
    summary: `
The beginning of this project. It's astute to kick it off with a beginning blog post. As I should.
`,
    slug: 'begin',
    component: import('../blog-posts/begin.tsx'),
  },
];
