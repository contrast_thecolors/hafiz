import { LoaderFunction, createBrowserRouter } from 'react-router-dom';
import Root from '@routes/root';
import BlogHome from '@src/routes/blog-home';
import blogPostConfig from './blog-post-config';
import { BlogPostLoaderData } from '@src/types/loader-data';
import BlogPost from '@src/routes/blog-post';
import HomePage from '@src/routes/home-page';

export const loaders: { [name: string]: LoaderFunction } = {
  blogHome: function (args) {
    const urlObj = new URL(args.request.url);
    const pageFromParams = urlObj.searchParams.get('page');

    return {
      pageFromParams,
    };
  },
  blogPost: function (args) {
    const slug = args.params.slug;
    const foundIndex = blogPostConfig.findIndex((item) => item.slug === slug);

    return {
      foundIndex,
    } as BlogPostLoaderData;
  },
};

export const router = createBrowserRouter([
  {
    path: '/',
    element: <Root />,
    children: [
      {
        path: '',
        loader: loaders.blogHome,
        element: <HomePage />,
      },
      {
        path: 'blog',
        loader: loaders.blogHome,
        element: <BlogHome />,
      },
      {
        path: 'blog/:slug',
        loader: loaders.blogPost,
        element: <BlogPost />,
      },
    ],
  },
]);
