import { MouseEvent } from 'react';
import { Link as ReactRouterLink, LinkProps } from 'react-router-dom';
// import { useSiteTopControlState } from '@src/store';

export default function Link(props: LinkProps) {
  const onClickPassed = props.onClick;
  // const { close } = useSiteTopControlState();
  const onClick = (evt: MouseEvent<HTMLAnchorElement>) => {
    // close();
    if (onClickPassed) {
      onClick(evt);
    }
  };
  return <ReactRouterLink {...props} onClick={onClick} />;
}
