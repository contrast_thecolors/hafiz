import clsx from 'clsx';
import styles from './avatar.module.scss';

interface Props {
  src: string;
  alt?: string;
  imgClassName: string;
}

export default function ({ src, alt, imgClassName }: Props) {
  return (
    // <div>
    <img
      className={clsx(styles.img_itself, imgClassName)}
      src={src}
      alt={alt || 'Profile Picture'}
    />

    // </div>
  );
}
