import { ReactNode } from 'react';
import clsx from 'clsx';
import styles from './BlogPostLayoutGeneral.module.scss';

export interface Props {
  title: string;
  containerClassName?: string;
  children: ReactNode;
}

export default function BlogPostLayoutGeneral({
  title,
  containerClassName,
  children,
}: Props) {
  return (
    <div className={clsx(styles.layout, containerClassName)}>
      <h3>{title}</h3>
      <div>{children}</div>
    </div>
  );
}
