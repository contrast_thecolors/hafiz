import styles from './SiteNav.module.scss';
import Link from '@src/components/Link';

export default function SiteNav() {
  return (
    <div className={styles.component}>
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/blog">Blog</Link>
          </li>
        </ul>
      </nav>
    </div>
  );
}
