declare const classNames: {
  readonly component: 'component';
  readonly info_row: 'info_row';
  readonly first_collapse_not_done: 'first_collapse_not_done';
  readonly collapsed: 'collapsed';
  readonly uncollapsed: 'uncollapsed';
  readonly info: 'info';
  readonly bottom_control_row: 'bottom_control_row';
};
export = classNames;
