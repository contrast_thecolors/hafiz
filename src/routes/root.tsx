import { Outlet } from 'react-router-dom';
import { IconContext } from 'react-icons';
import { RiGitCommitLine } from 'react-icons/ri';
// import SiteTopControl from '@site-pieces/SiteTopControl';
import SiteNav from '@site-pieces/SiteNav';

import '@src/index.scss';

function Root() {
  return (
    <div className="master-container">
      <div className="content-container">
        {/* <SiteTopControl /> */}
        <SiteNav />
        <Outlet />
      </div>
      <div className="site-bottom">
        <IconContext.Provider value={{ size: '2rem', color: '#efefef' }}>
          <RiGitCommitLine />
        </IconContext.Provider>
      </div>
    </div>
  );
}

export default Root;
