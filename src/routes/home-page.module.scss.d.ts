declare const classNames: {
  readonly page: 'page';
  readonly avatar_img: 'avatar_img';
  readonly contact_box: 'contact_box';
};
export = classNames;
