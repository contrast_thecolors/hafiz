import { useState, useMemo, lazy as reactLazy, useEffect } from 'react';
import { BlogPostLoaderData } from '@src/types/loader-data';
import { Suspense } from 'react';
import { useLoaderData } from 'react-router-dom';
import blogPostConfig from '@src/config/blog-post-config';

export default function BlogPost() {
  const [index, setIndex] = useState<null | number>(null);
  const BlogPost = useMemo(() => {
    if (index === null) return null;
    return reactLazy(() => blogPostConfig[index as number].component);
  }, [index]);
  const data = useLoaderData() as BlogPostLoaderData;
  console.log({ data });
  const { foundIndex } = data;
  useEffect(() => {
    setIndex(foundIndex);
  }, [foundIndex]);

  if (!BlogPost) return <div>Not Found</div>;

  return (
    <Suspense fallback={<div>Loading...</div>}>
      <BlogPost />
    </Suspense>
  );
  return <div>Blog Post</div>;
}
