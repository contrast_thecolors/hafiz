import { Link } from 'react-router-dom';
import blogPostConfig from '@src/config/blog-post-config';
import styles from './blog-home.module.scss';

export default function BlogHome() {
  return (
    <div className={styles.listing}>
      {blogPostConfig.map(({ title, slug, summary }) => (
        <div className={styles.listing_item}>
          <h3>
            <Link to={`/blog/${slug}`}>{title}</Link>
          </h3>
          <div>{summary}</div>
        </div>
      ))}
    </div>
  );
}
