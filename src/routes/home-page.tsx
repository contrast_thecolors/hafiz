import Avatar from '@src/components/Avatar';
import pfpOneSrc from '@assets/pfp_one.jpeg';
import styles from './home-page.module.scss';
import { DISC_SERVER_PERM, EMAIL } from '@src/config/social';

export default function HomePage() {
  return (
    <div className={styles.page}>
      <Avatar
        src={pfpOneSrc}
        alt="Profile Pic (used in my blog/website)"
        imgClassName={styles.avatar_img}
      />
      <p>
        You can call me Hafiz or حافظ, the former being the pronouncidation in
        the Urdu language. I am an occultist. I delve into the occult. My
        current interests include chaos magick, sigil magic, ceremonial magick,
        prayers and rites, bilocation/partial-projection, and scanning. What
        this website is meant to be, is a means to share ideas I have. As such
        the blog element is crucial.
      </p>
      <p>
        I also do programming, and intend to create things that are meant to be
        benefical to those in the occult.
      </p>
      <p>
        My main priority is people being able to take care of themselves, and to
        address a number of psychological issues. Now, some may argue that
        internal conflict is not always psychological, but these too can be
        resolved in the psyche. But where it is not, that is meant to be found
        and addressed.
      </p>
      <p>
        I am not an expert, but I'm someone who had made many mistakes. And I do
        not wish for others to be less fortunate. There were things I could have
        done differently, dilema that I could have processed differently. There
        were bad decisions I could have averted. These things, I wish to write
        about.
      </p>
      <p>
        Additionally, I am very much interested in the preservation of certain
        languages and cultural knowledge bases. Indeed this seems intwined with
        Arabic, as is with Islam and Muslim culture. Though worry not. I do not
        function from a particular religious framework and while there may be
        material that makes use of these areas that I have interest in and
        intend to preserve and propogate, this is not my entirety.
      </p>
      <p>
        My interests also do not necessarily reflect my position regarding any
        issue. I follow my own justice/dharma but for quite a bit I am inspired
        by the directives in various religions, as I am by the choices made by
        people around me.
      </p>
      <p>You can get to know me better and submit feedback.</p>
      <div className={styles.contact_box}>
        <h3>Contact Box</h3>
        <p>You can write to me at {EMAIL}</p>
        <p>
          You can also join my chill discord server:{' '}
          <a href={DISC_SERVER_PERM} target="_blank">
            here
          </a>
        </p>
      </div>
    </div>
  );
}
