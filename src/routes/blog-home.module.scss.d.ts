declare const classNames: {
  readonly listing: 'listing';
  readonly listing_item: 'listing_item';
};
export = classNames;
